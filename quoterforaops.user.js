// ==UserScript==
// @name        Quoter for AoPS
// @namespace   Violentmonkey Scripts
// @match       https://artofproblemsolving.com/*
// @grant       none
// @version     1.0
// @author      happycupcake/epiccakeking
// @description 7/21/2020, 6:32:54 PM
// ==/UserScript==
AoPS.Community.Views.Post.prototype.onClickQuote=function() {
  this.topic.appendToReply("[hide=#"+this.model.get("post_number")+":][quote name=\"" +this.model.get("username") +"\" url=\"/community/p"+this.model.get("post_id")+"\"]\n" +this.model.get("post_canonical").trim()+"\n[/quote][/hide] ");
}